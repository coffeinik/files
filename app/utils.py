# -*- coding: utf-8 -*-
from app import app, db
from flask import logging
import os
from app.models import File, User
from flask.ext.security.utils import md5
from sqlalchemy import not_
from werkzeug.datastructures import FileStorage


files_limit = app.config["FILES_LIMIT"]
folder = app.config['UPLOAD_FOLDER']
logger = logging.getLogger('files')


def is_not_exceed_limit(user):
    """
    Verifies that the user does not exceed the limit of files
    :param user: instance of app.models.User object or user id as integer
    """
    if isinstance(user, User):
        user_id = user.id
    elif isinstance(user, int):
        user_id = user
    else:
        raise TypeError("user should be an instance of User or integer")

    user_files_count = File.query.filter(File.user_id == user_id).count()

    return user_files_count < files_limit


def save_file(file_object, filename, user):
    """
    :param file_object: flask FileStorage object
    :param filename: hashed filename
    :param user: instance of app.models.User object or user id as integer
    """
    if not isinstance(file_object, FileStorage):
        raise TypeError("file should be FileStorage type")
    if isinstance(user, User):
        user_id = user.id
    elif isinstance(user, int):
        user_id = user
    else:
        raise TypeError("user should be an instance of User or integer")

    file_object.save(os.path.join(folder, filename))
    file_model = File(
        user_id=user_id,
        name=file_object.filename,
        link=filename
    )
    db.session.add(file_model)
    db.session.commit()

    return filename


def delete_file(file_model):
    """
    :param file_model: instance of app.models.File object or
    """
    if not isinstance(file_model, File):
        if isinstance(file_model, int):
            file_model = File.query.filter(File.id == file_model).one()
        else:
            raise TypeError("user should be an instance of User or integer")

    count_identity_files = File.query.filter(File.link == file_model.link).count()

    if count_identity_files == 1:
        os.remove(folder + file_model.link)

    db.session.delete(file_model)
    db.session.commit()


def generate_filename(file_object):
    """
    Generate filename from file's hash
    :param file_object: flask FileStorage object
    :return: hashed filename
    """
    filename = md5(file_object.read())
    file_object.seek(0)
    return filename


def is_file_doubling(filename):
    """
    Verifies that the file is not unique
    :param filename: real file path
    :return: boolean
    """
    files = File.query.filter(File.link == filename).count()
    return files > 0


def users_already_have_file(filename):
    """
    Returns the users who already have this file
    :param filename: real file path
    :return: list of app.models.User objects
    """
    files_subquery = File.query.filter(File.link == filename).subquery()
    users = User.query.filter(User.id == files_subquery.c.user_id)
    return users.all()
