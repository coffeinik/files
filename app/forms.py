# -*- coding: utf-8 -*-
from flask_security.forms import RegisterForm as BaseRegisterForm
from wtforms import StringField, validators


class RegisterForm(BaseRegisterForm):
    username = StringField('Login', validators=[
        validators.DataRequired(),
        validators.Length(min=4, max=12),
    ])
