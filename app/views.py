# -*- coding: utf-8 -*-
from flask import render_template, request, redirect, url_for, flash
from flask import send_from_directory, logging, abort
from flask.ext.login import current_user, login_required
from sqlalchemy.orm.exc import NoResultFound

from app import app
from app.models import File
from app.utils import save_file, delete_file, is_not_exceed_limit, \
    is_file_doubling, generate_filename, users_already_have_file


logger = logging.getLogger("files")


@app.route('/')
def index():
    if current_user.is_authenticated():
        File.query.filter(File.user_id == current_user.id).count()
        files = File.query.filter(File.user_id == current_user.id)
        return render_template("index.html", **{
            "files": files
        })
    return render_template("index.html")


@login_required
@app.route("/file-upload", methods=['POST'])
def file_upload():
    if is_not_exceed_limit(current_user.id):
        file_object = request.files['file']
        if file_object:
            filename = generate_filename(file_object)

            if is_file_doubling(filename):

                users = users_already_have_file(filename)
                user_names = username_list(users, current_user.id)
                if user_names:
                    flash(
                        "This users have this file: " + user_names,
                        "info"
                    )

            save_file(file_object, filename, current_user.id)

            flash("Successful upload", "success")
        else:
            flash("You need insert a file", "error")
    else:
        flash("You have exceeded the limit", "error")
    return redirect(url_for('index'))


@app.route("/file-download/<file_id>", methods=['GET'])
def file_download(file_id):
    try:
        file_model = File.query.get(file_id)
        return send_from_directory(
            app.config['UPLOAD_FOLDER'],
            file_model.link,
            as_attachment=True,
            attachment_filename=file_model.name.encode('utf-8')
        )
    except ValueError:
        logger.error("file id should be int")
    except NoResultFound:
        logger.error("No result for file with id {}".format(file_id))
        abort(404)


@login_required
@app.route("/file-delete/<file_id>", methods=['GET'])
def file_delete(file_id):
    try:
        delete_file(int(file_id))
    except ValueError:
        logger.error("file id should be int")
    except Exception as e:
        logger.error(e.message)
        raise e

    return redirect(url_for("index"))


def username_list(users, current_user_id):
    users = filter(lambda user: user.id != current_user_id, users)
    names = map(lambda user: user.username, users)
    return ", ".join(names)
